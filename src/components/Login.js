import { useState, useContext, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import AppHelper from '../components/app-helper';

export default function index() {

    return ( 
        <Form title={ 'Login' }>
            <Row className="justify-content-center">
                <Col>
                    <LoginForm />
                </Col>
            </Row>
        </Form>
    )

}

const LoginForm = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

//
    function authenticate(e) {
        e.preventDefault();
        
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }

        fetch(`${AppHelper.API_URL}/users/login`, options) 
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data);

            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
            } else {
                if(data.error === 'does-not-exist'){
                    alert('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password'){
                    alert('Authentication Failed', 'Password is incorrect.', 'error')
                }
            }
        })
    }       


    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [ email, password ])

    return (
        <Card>
            <Card.Body>
                <h4 className="text-center">E-Commerce Login</h4>
                <Form onSubmit={ authenticate }/>

                    <Form.Group controlId="userEmail">
                        <Form.Control 
                            type="email"
                            placeholder="Email Address" 
                            value={ email } 
                            onChange={ (e) => setEmail(e.target.value) } 
                            autoComplete="off" 
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Control 
                            type="password"
                            placeholder="Password" 
                            value={ password } 
                            onChange={ (e) => setPassword(e.target.value) } 
                            required
                        />
                    </Form.Group>

                    <Button 
                        className="rounded-pill"
                        variant="primary"
                        type="submit" 
                        block
                    >
                        Login
                    </Button>

                    <h6 className="text-center">No Account?<a href="./components/Register">Register Now!</a></h6>
            </Card.Body>
        </Card>
    )
}