import React from 'react'
import styled from 'styled-components'
import homeImage from '../images/about.jpg'

const Section = styled.section`
    height: 70vh;
    background: #CAD5CA;
    padding: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;

    @media only screen and (max-width: 360px){
        height: 100vh;
    }
`

const Container = styled.div`
    width: 90%;
    height: 70vh;
    display: flex;
    flex-direction: row;
    align-items: center;

    @media only screen and (max-width: 360px){
        flex-direction: column-reverse;
    }
`

const LeftContainer = styled.div`
    width: 50%;
    height: 50vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 4rem;

    h1{
        margin: 2rem 0;
        text-transform: uppercase;
        border-bottom: 4px solid #211A1D;
    }
    
    @media only screen and (max-width: 360px){
        width: 160%;
        margin: 1rem 0;
    }
`

const RightContainer = styled.div`
    width: 50%;
    height: 50vh;
    display: flex;
    align-items: center;
    justify-content: center;

    img{
        width: 70%;  
        border-radius: 10px;
        box-shadow: 0 35px 20px #777;
    }

    @media only screen and (max-width: 360px){
        width: 140%;
    }
`
export default function About() {
    return (
        <Section>
            <Container>
                <LeftContainer>
                    <h1>Lorem Ipsum</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                        ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate 
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </LeftContainer>
                <RightContainer>
                    <img src={homeImage} alt="" />
                </RightContainer>
            </Container>
        </Section>
    )
}
