import React from 'react'
import Products from './Products'
import {ProductsData} from '../data/ProductsData'
import styled from 'styled-components'

const Section = styled.section`
    height: 110vh;
    padding: 5rem;
    
    @media only screen and (max-width: 360px){
        padding: 0;
        height: auto;
    }
`

const Container = styled.div`
    width: 100%;
    height: 110%;
    display: flex;
    flex-wrap: wrap;
    justify-content: start;

    @media only screen and (max-width: 360px){
        flex-direction: column;
    }
`

export default function ProductList({addToCart}) {

    return (
        <Section>
            <Container>
                {ProductsData.map((product, index) => {
                        return(
                            <Products 
                                key={index}
                                description={product.description}
                                image={product.image}
                                price={product.price}
                                addToCart={addToCart}
                            />
                        )
                    })
                }
            </Container>
        </Section>
    )
}
