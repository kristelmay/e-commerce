import React from 'react'
import styled from 'styled-components'
import {GrFacebook} from 'react-icons/gr'
import {SiMonster} from 'react-icons/si'
import {FaLinkedin} from 'react-icons/fa'
import { Element } from 'react-scroll'
import { ExternalLink } from 'react-external-link'

const TopBar = styled.div`
	width: 100%;
	height: 7vh;
	background: #211A1D;
	display: flex;
	justify-content: space-around;
	text-align: center;
	align-items: center;
	color: #F8F0FB;

	@media screen and (max-width: 360px){
		height: 10vh;
	}

	p{
		@media screen and (max-width: 360px){
			font-size: 0.8rem;
		}
	}
`


const IconContainer = styled.div`
	display: flex;
	justify-content: space-around;
`

const Icons = styled(ExternalLink)`
	font-size: 2rem;
	margin: 0 2rem;
	cursor: pointer;
	color: #e2e2e2;

	&:hover{
  		transform: scale(1.2);
  	}

	@media screen and (max-width: 360px){
		font-size: 1rem;
		margin: 0 0.5rem;
	}
`

const Container = styled.div`
	height: 30vh;
	display: flex;
	display: flex;
  	flex-wrap: wrap;
  	flex-direction: row;
  	justify-content: space-around;
  	align-items: center;

  	@media screen and (max-width: 360px){
		font-size: 0.8rem;
		margin: 0.5rem;
	}
`

const ColumnContainer = styled.div`
	display: flex;
	flex-direction: column;
  	justify-content: space-around;
  	text-align: center;

  	h4{
  		margin: 1rem 0;
  		font-size: 1.1rem;

  		@media screen and (max-width: 360px){
			font-size: 0.8rem;
			margin: 0;
		}
  	}

  	p{
  		margin: 1rem 0;

  		@media screen and (max-width: 360px){
			font-size: 0.8rem;
			margin: 0.2rem;
		}
  	}
`


const Footer = () => {

	return(
		<Element id="contact">
			<TopBar>
				<p>Get connected with me on social networks!</p>
				<IconContainer>
					<Icons>
						<GrFacebook />
					</Icons>
					<Icons>
						<SiMonster />
					</Icons>
					<Icons>
						<FaLinkedin/>
					</Icons>
				</IconContainer>
			</TopBar>
			<Container>
				<ColumnContainer>
					<h4>ECOMMERCE</h4>
					<p>Catering to your style</p>
					<p>needs from A-Z.</p>
				</ColumnContainer>
				<ColumnContainer>
					<h4>CONTACT</h4>
					<p>09282737733</p>
					<p>sample0@gmail.com</p>
					<p>sample@gmail.com</p>
				</ColumnContainer>
				<ColumnContainer>
					<h4>Service</h4>
					<p>Online Store</p>
				</ColumnContainer>
			</Container>
		</Element>
	)
}

export default Footer
