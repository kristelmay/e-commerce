import React, {useState} from 'react'
import styled from 'styled-components'
import {SliderData} from '../data/SliderData'
import {FaArrowAltCircleRight, FaArrowAltCircleLeft} from 'react-icons/fa'

const Section = styled.section`
    position: relative;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;

    @media only screen and (max-width: 360px){
        height: 100vh;
    }

`
const LeftArrow = styled(FaArrowAltCircleLeft)`
    position: absolute;
    left: 10%;
    font-size: 3rem;
    top: 50%;
    color: #000;
    z-index: 10;
    cursor: pointer;
    user-select: none;
    @media only screen and (max-width: 360px){
        top: 63%;
    }
`

const RightArrow = styled(FaArrowAltCircleRight)`
    position: absolute;
    right: 10%;
    top: 50%;
    font-size: 3rem;
    color: #000;
    z-index: 10;
    cursor: pointer;  
    user-select: none;
    @media only screen and (max-width: 360px){
        top: 63%;
    }
`

const Img = styled.img`
    width: 100%;
    height: 400px;
    border-radius: 10px;
    box-shadow: 0 35px 20px #777;
    @media only screen and (max-width: 360px){
        height: 40vh;
    }
`

const RightContainer = styled.div`
    width: 50%;
    height: 50vh;
    display: flex;
    align-items: center;
    justify-content: start;
    padding: 0 0 0 2rem;

    @media only screen and (max-width: 360px){
        width: 130%;
        margin-top: 3rem;
    }

`

const LeftContainer = styled.div`
    width: 50%;
    height: 50vh;
    display: flex;
    align-items: center;
    justify-content: center;

    @media only screen and (max-width: 360px){
        width: 120%;
        margin-top: 8rem;
    }
`

const ColContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const DescContainer = styled.div`
    width: 90%;
    height: 70vh;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;

    @media only screen and (max-width: 360px){
        flex-direction: column;
        width: 100%;
        height: 100vh;
    }
`

const Slide= styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 70%;
    
`

const Line = styled.div`
    border-left: 1px solid #211A1D;
    height: 500px;
    margin: 0 0 0 3rem;
    
    @media only screen and (max-width: 360px){
        display: none;
    }
`

export default function Slider({slides}) {

    const [current, setCurrent] = useState(0)
    const length = slides.length

    const nextSlide = () => {
        setCurrent(current === length - 1 ? 0 : current + 1)
    }

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 1 : current - 1)
    }

    if(!Array.isArray(slides)  || slides.length <= 0){
        return null;
    }
    

    return (
        <Section>
            <ColContainer>
                <LeftArrow onClick={prevSlide}/> 
                <RightArrow onClick={nextSlide} />
                {SliderData.map((slide, index) => {
                        return (
                            <Slide key={index}>
                                {index === current &&
                                    <DescContainer>
                                        <LeftContainer>
                                            <Img src={slide.image} />
                                        </LeftContainer>
                                        <Line></Line>
                                        <RightContainer>
                                            <p>{slide.description}</p>
                                        </RightContainer>
                                    </DescContainer>
                                }
                            </Slide>
                        )
                    })
                }
            </ColContainer>
        </Section>
    )
}
