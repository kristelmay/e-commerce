import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    max-width: 80%;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    flex-wrap: nowrap;

    img{
        width: 10%;
        height: 10vh;
    }

    div{
        width: 80%;
    }
`

export default function CheckoutPage({price, description, image}) {
    return (
        <Container>
            <img src={image} alt=""/>
            <div>
                <h5>{description}</h5>
                <h5>{price}</h5>
            </div>
        </Container>
    )
}
