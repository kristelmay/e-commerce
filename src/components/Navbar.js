import React, {useState} from 'react';
import styled, {css} from 'styled-components/macro'
import {VscThreeBars} from 'react-icons/vsc'
import { Link } from 'react-scroll'
import {AiOutlineShoppingCart} from 'react-icons/ai'
import CheckoutPage from './CheckoutPage';

const Nav = styled.nav`
	height: 60px;
	background: #211A1D;
	display: flex;
	justify-content: flex-start;
	padding: 1rem 2rem;
	z-index: 100;
	position: fixed;
	width: 100%;
	box-shadow: 0px 0 10px rgba(0, 0, 0, 0.8);
`

const NavLink = css`
	color: #F8F0FB;
	display: flex;
	align-items: center;
	padding: 0 1.3rem;
	height: 100%;
	cursor: pointer;
	text-transform: uppercase;
	font-weight: 700;
	text-decoration: none;

	h3{
		background: #6320EE;
		width: 1.2rem;
		border-radius: 50px;
	}
`

const Logo = styled(Link)`
	${NavLink}
	color: #8075FF;
	font-weight: 700;
`

const MenuBars = styled(VscThreeBars)`
	display: block;
	height: 40px;
	width: 40px;
	cursor: pointer;
	position: absolute;
	top: 0;
	right: 0;
	transform: translate(-50%, 25%);
	color: #F8F0FB;
`

const NavMenu = styled.div`
	display: flex;
	align-items: center;
	margin-left: 33rem;

	@media screen and (max-width: 768px){
		display: none
	}
`

const NavMenuLinks = styled(Link)`
	${NavLink}
`

const Button = styled.button`
	width: 25%;
	padding: 15px 25px;
	cursor: pointer;
	outline: none;
	color: #e2e2e2;
	background: #6320EE;
	border: none;
	border-radius: 4px;
	margin-left: 3rem;
	font-weight: 700;

	&:hover {
		transform: scale(1.1);
	  }
`

const SearchContainer = styled.div`
    display: flex;
    align-items: center;
    width: 10%;
`

const Input = styled.input`
    background: #e2e2e2;
    border: none;
    border-radius: 4px;
    outline: none;
    padding: 10px;

`

const Cart = styled(AiOutlineShoppingCart)`
	font-size: 1rem;
`

const LeftContainer = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
`

const Checkout = styled.div`
	position: fixed;
	z-index: 999;
	width: 100%;
	height: auto;
	background: #CAD5CA;
	display: grid;
	color: #211A1D;
	align-items: center;
	justify-content: center;
	top: 0;
	left: 0;
	transition: 0.3s ease-in-out;
	opactiy: 1;
	top: 3.7rem;
`

const Navbar = ({toggle, count, products}) => {

	const [isOpen, setIsOpen] = useState(false)

	const CheckoutToggle = () => {
		setIsOpen(!isOpen)
	}

	return(
		<Nav>
			<LeftContainer>
				<Logo to="/">eCommerce</Logo>
				<SearchContainer>
						<Input 
						placeholder="Search"
					/>
				</SearchContainer> 
			</LeftContainer>
			<MenuBars onClick={toggle}/>
			<NavMenu>
				<NavMenuLinks>
					MEN
				</NavMenuLinks>
				<NavMenuLinks>
					WOMEN
				</NavMenuLinks>
				<NavMenuLinks>
					KIDS
				</NavMenuLinks>
				<NavMenuLinks>
					GADGETS
				</NavMenuLinks>
				<NavMenuLinks>
 					<Cart onClick={CheckoutToggle}/>
					<h3>{count}</h3>
					{isOpen === false ? null : 
					<Checkout>
						{products.length === 0 ? setIsOpen(false) :
							products.map((element, index) => {
								return (
									<CheckoutPage
										image={element.product.image}
										price={element.product.price}
										description={element.product.description}
									/>
								)
							})
						}
					</Checkout>		
					}
				</NavMenuLinks>
				<Button onclick="./src/components/Login">SIGN IN</Button>
				</NavMenu>
				</Nav>
	)
}

export default Navbar;