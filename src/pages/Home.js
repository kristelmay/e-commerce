import React, {useState} from 'react'
import { SliderData } from '../data/SliderData'
import Slider from '../components/Slider'
import styled from 'styled-components'
import About from '../components/About'
import ProductList from '../components/ProductList'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import Dropdown from '../components/Dropdown'
import Register from '../components/Register'

const Container = styled.div`
`

export default function Home() {

    const [isOpen, setIsOpen] = useState(false)
    const [count, setCount] = useState(0);
    const [products, setProducts] = useState([])

	const toggle = () => {
		setIsOpen(!isOpen)
	}

    const addToCart = (product) => {
        setCount(count + 1)
        const newProduct = [product, ...products];
        setProducts(newProduct)
    }

    return (
        <Container>
            <Navbar toggle={toggle} count={count} products={products}/>
      	    <Dropdown isOpen={isOpen} toggle={toggle}/>
            <Slider slides={SliderData}/>  
            <About />
            <ProductList addToCart={addToCart} />
            <Footer />
            <Register />
		</Container>
    )
}
