import React, {useState} from 'react'
import styled from 'styled-components'

const Container = styled.div`
    width: 15%;
    margin: 0 2rem;
    height: 20vh;
    border-radius: 10px;
    background: #e0e0e0;
    cursor: pointer;
    box-shadow:  20px 30px 50px #bebebe,
                -20px -20px 60px #ffffff;

    &:hover {
        transform: scale(1.1);
    }            
    
    @media only screen and (max-width: 360px){
        width: 100%;
        margin: 0;
        height: 60vh;
    }           
`

const ImageContainer = styled.div`
    padding 1rem;
    img{
        width: 100%;
        height: 200px;
        border-radius: 10px;
    }

    @media only screen and (max-width: 360px){
        img{
            height: 30vh;
        }
    } 
`

const Description = styled.div`
    display: flex;
    padding: 1rem;
    justify-content: center;
    flex-direction: column;
    align-text: center;
    align-items: center;
    border-radius: 10px;
    background: #e0e0e0;
    box-shadow:  20px 30px 50px #bebebe,
                -20px -20px 60px #ffffff;
    p{
        margin: .2rem;
    }
    
`

const ProductContainer = styled.div`
    position: relative;
    left: -5rem;
    width: 27vw;
    height: 70vh;
    background: #CAD5CA;
    padding: 0 2rem 0 3rem;
    display: flex;
    align-items: center;
    border-radius: 10px;
    justify-content: space-around;
    flex-direction: column;
    transition: 0.3s ease-in-out;
    img{
        width: 90%;
        height: 45vh;
        border-radius: 10px;
    }

    button{
        padding: 15px 25px;
        margin: 0.4rem;
        cursor: pointer;
        outline: none;
        color: #F8F0FB;
        background-color: #211A1D;
        border: none;
        border-radius: 10px;
        text-transform: uppercase;
    }
`

export default function Products({description, image, price, addToCart}) {

    const [display, setDisplay] = useState(false);
    const [product, setProduct] = useState({
        description: description,
        image: image,
        price: price
    })

    const toggle = (product) => {
		setDisplay(!display)
        
	}

    const handleClick = () => {
        addToCart({
            product
        })
    }

    return (
        <Container onClick={toggle}>
            {display === false ? null : 
                <ProductContainer>
                        <img src={image} alt="" />
                        <p>{description}</p>
                        <p>{price}</p>
                        <button onClick={handleClick}>Add to Cart</button>
                </ProductContainer>}
            <ImageContainer>
                <img src={image} alt="" />
            </ImageContainer>
            <Description>
                <p>{description}</p>
                <p>{price}</p>
            </Description>
        </Container>
    )
}
