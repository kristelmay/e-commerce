import React from 'react'
import styled from 'styled-components'
import {menuData} from '../data/MenuData'
import {FaTimes} from 'react-icons/fa'
import { Link } from 'react-scroll'

const DropdownContainer = styled.div`
	position: fixed;
	z-index: 999;
	width: 100%;
	height: 100%;
	background: #211A1D;
	display: grid;
	align-items: center;
	top: 0;
	left: 0;
	transition: 0.3s ease-in-out;
	opactiy: ${({isOpen}) => (isOpen ? '1' : '0')};
	top: ${({isOpen}) => (isOpen ? '0' : '-100%')};
`

const Icon = styled.div`
	position: absolute;
	top: 1.2rem;
	right: 1.5rem;
	background: transparent;
	font-size: 2rem;
	cursor: pointer;
	outline;
`

const CloseIcon = styled(FaTimes)`
	color: #F8F0FB;
`
const DropdownWrapper = styled.div``

const DropdownMenu = styled.div`
	display: grid;
	git-template-columns: 1fr;
	grid-template-rows: repeat(4, 80px);
	text-align: center;
	margin-bottom: 4rem;

	@media screen and (max-width: 480px){
		grid-template-rows: repeat(4, 60px);
	}
`

const DropdownLink = styled(Link)`
	display: flex;
	color: #F8F0FB;
	align-items: center;
	justify-content: center;
	font-size: 1.5rem;
	text-decoration: none;
	list-style: none;
	cursor: pointer;
	transition: 0.2s ease-in-out;
	text-transform: uppercase;

	&:hover{
		color: #000d1a;
	}
`


const Dropdown = ({isOpen, toggle}) => {
	return(
		<DropdownContainer isOpen={isOpen} onClick={toggle}>
			<Icon onClick={toggle}>
				<CloseIcon />
			</Icon>
			<DropdownWrapper>
				<DropdownMenu>
					{menuData.map((item, index)=> (
						<DropdownLink onClick={toggle} to={item.link} key={index}>
							{item.title}
						</DropdownLink>
					))}
				</DropdownMenu>
			</DropdownWrapper>
		</DropdownContainer>
	)
}

export default Dropdown